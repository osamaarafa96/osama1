-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2018 at 11:08 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ciblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `user_id`, `name`, `created_at`) VALUES
(1, 1, 'Business', '2018-03-30 13:09:30'),
(2, 2, 'Technology', '2018-03-30 13:09:30'),
(3, 1, 'Sports', '2018-03-31 19:24:10');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `username`, `body`, `created_at`) VALUES
(2, 2, 3, 'osamaarafa96', 'I will try to use this technology nearly .', '2018-04-01 10:02:27'),
(3, 2, 5, 'osamaarafa96', 'nice post', '2018-04-03 10:27:09');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(50) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`) VALUES
(1, 'Cambodia'),
(2, 'Thailand'),
(3, 'China');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `province_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `post_image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `province_id`, `category_id`, `user_id`, `title`, `slug`, `body`, `post_image`, `created_at`) VALUES
(1, 1, 1, 1, 'Blog Post 1', 'Blog-Post-1', '<p><em><strong>Iron Man</strong></em>&nbsp;is a 2008 American&nbsp;<a href="https://en.wikipedia.org/wiki/Superhero_film">superhero film</a>&nbsp;based on the&nbsp;<a href="https://en.wikipedia.org/wiki/Marvel_Comics">Marvel Comics</a>&nbsp;character&nbsp;<a href="https://en.wikipedia.org/wiki/Iron_Man">of the same name</a>, produced by&nbsp;<a href="https://en.wikipedia.org/wiki/Marvel_Studios">Marvel Studios</a>and distributed by&nbsp;<a href="https://en.wikipedia.org/wiki/Paramount_Pictures">Paramount Pictures</a>.<a href="https://en.wikipedia.org/wiki/Iron_Man_(2008_film)#endnote_1">1</a>&nbsp;It is&nbsp;<a href="https://en.wikipedia.org/wiki/List_of_Marvel_Cinematic_Universe_films">the first film</a>&nbsp;in the&nbsp;<a href="https://en.wikipedia.org/wiki/Marvel_Cinematic_Universe">Marvel Cinematic Universe</a>&nbsp;(MCU). The film was directed by&nbsp;<a href="https://en.wikipedia.org/wiki/Jon_Favreau">Jon Favreau</a>, with a screenplay by the writing teams of&nbsp;<a href="https://en.wikipedia.org/wiki/Mark_Fergus_and_Hawk_Ostby">Mark Fergus and Hawk Ostby</a>&nbsp;and&nbsp;<a href="https://en.wikipedia.org/wiki/Art_Marcum_and_Matt_Holloway">Art Marcum and Matt Holloway</a>. It stars&nbsp;<a href="https://en.wikipedia.org/wiki/Robert_Downey_Jr.">Robert Downey Jr.</a>&nbsp;as Tony Stark / Iron Man, alongside&nbsp;<a href="https://en.wikipedia.org/wiki/Terrence_Howard">Terrence Howard</a>,&nbsp;<a href="https://en.wikipedia.org/wiki/Jeff_Bridges">Jeff Bridges</a>,&nbsp;<a href="https://en.wikipedia.org/wiki/Shaun_Toub">Shaun Toub</a>, and&nbsp;<a href="https://en.wikipedia.org/wiki/Gwyneth_Paltrow">Gwyneth Paltrow</a>. In&nbsp;<em>Iron Man</em>, Tony Stark, an industrialist and master engineer, builds a&nbsp;<a href="https://en.wikipedia.org/wiki/Iron_Man%27s_armor_in_other_media#Live_action">powered exoskeleton</a>&nbsp;and becomes the technologically advanced&nbsp;<a href="https://en.wikipedia.org/wiki/Superhero">superhero</a>&nbsp;Iron Man.</p>\r\n\r\n<p>The film had been in development since 1990 at&nbsp;<a href="https://en.wikipedia.org/wiki/Universal_Studios">Universal Pictures</a>,&nbsp;<a href="https://en.wikipedia.org/wiki/20th_Century_Fox">20th Century Fox</a>, or&nbsp;<a href="https://en.wikipedia.org/wiki/New_Line_Cinema">New Line Cinema</a>&nbsp;at various times, before Marvel Studios reacquired the rights in 2006. Marvel put the project in production as its first self-financed film, with Paramount Pictures as its distributor. Favreau signed on as director, aiming for a naturalistic feel, and he chose to shoot the film primarily in California, rejecting the&nbsp;<a href="https://en.wikipedia.org/wiki/East_Coast_of_the_United_States">East Coast</a>&nbsp;setting of the comics to differentiate the film from numerous superhero films set in New York City-esque environments. Filming began in March 2007 and concluded in June. During filming, the actors were free to create their own dialogue because pre-production was focused on the story and action. Rubber and metal versions of the armors, created by&nbsp;<a href="https://en.wikipedia.org/wiki/Stan_Winston">Stan Winston</a>&#39;s company, were mixed with&nbsp;<a href="https://en.wikipedia.org/wiki/Computer-generated_imagery">computer-generated imagery</a>&nbsp;to create the title character.</p>\r\n\r\n<p><em>Iron Man</em>&nbsp;premiered in&nbsp;<a href="https://en.wikipedia.org/wiki/Sydney">Sydney</a>&nbsp;on April 14, 2008, and was released in the United States on May 2, 2008. The film was a critical and commercial success, grossing over $585 million and garnering critical acclaim. Downey&#39;s performance as Tony Stark was particularly praised. The&nbsp;<a href="https://en.wikipedia.org/wiki/American_Film_Institute">American Film Institute</a>&nbsp;selected the film as one of the ten best of the year. It was also nominated for two&nbsp;<a href="https://en.wikipedia.org/wiki/Academy_Awards">Academy Awards</a>&nbsp;for Best Sound Editing and Best Visual Effects. Two sequels,&nbsp;<em><a href="https://en.wikipedia.org/wiki/Iron_Man_2">Iron Man 2</a></em>&nbsp;and&nbsp;<em><a href="https://en.wikipedia.org/wiki/Iron_Man_3">Iron Man 3</a></em>, were released on May 7, 2010, and&nbsp;</p>\r\n', 'Ironmanposter.JPG', '2018-03-31 22:56:02'),
(2, 4, 3, 1, 'Blog Post 2', 'Blog-Post-2', '<p><strong>Mohamed Salah Ghaly</strong>&nbsp;(<a href="https://en.wikipedia.org/wiki/Egyptian_Arabic_language">Egyptian Arabic</a>:&nbsp;محمد صلاح غالى&lrm; &nbsp;<small>pronounced&nbsp;</small><a href="https://en.wikipedia.org/wiki/Help:IPA/Egyptian_Arabic">[m&aelig;ˈħam&aelig;d sˤɑˈlɑːħ ˈɣ&aelig;ːli]</a>; born 15 June 1992) is an Egyptian professional&nbsp;<a href="https://en.wikipedia.org/wiki/Association_football">footballer</a>&nbsp;who plays as a&nbsp;<a href="https://en.wikipedia.org/wiki/Forward_(association_football)">forward</a>&nbsp;for&nbsp;<a href="https://en.wikipedia.org/wiki/Liverpool_F.C.">Liverpool</a>&nbsp;and the&nbsp;<a href="https://en.wikipedia.org/wiki/Egypt_national_football_team">Egyptian national team</a>.</p>\r\n\r\n<p>Salah started his club career with&nbsp;<a href="https://en.wikipedia.org/wiki/El_Mokawloon_SC">El Mokawloon</a>&nbsp;in the&nbsp;<a href="https://en.wikipedia.org/wiki/Egyptian_Premier_League">Egyptian Premier League</a>&nbsp;in 2010 before moving to&nbsp;<a href="https://en.wikipedia.org/wiki/FC_Basel">FC Basel</a>&nbsp;in the&nbsp;<a href="https://en.wikipedia.org/wiki/Swiss_Super_League">Swiss Super League</a>&nbsp;in 2012. He won the Swiss League in his first season with Basel and was awarded the&nbsp;<a href="https://en.wikipedia.org/wiki/Swiss_Super_League">SAFP Golden Player award</a>for being the best player in the League. He then played for&nbsp;<a href="https://en.wikipedia.org/wiki/Chelsea_F.C.">Chelsea</a>&nbsp;in the&nbsp;<a href="https://en.wikipedia.org/wiki/Premier_League">Premier League</a>, winning the&nbsp;<a href="https://en.wikipedia.org/wiki/Premier_League_2014%E2%80%9315">2014&ndash;15 Premier League</a>, before moving to&nbsp;<a href="https://en.wikipedia.org/wiki/Serie_A">Serie A</a>&nbsp;in Italy, joining&nbsp;<a href="https://en.wikipedia.org/wiki/ACF_Fiorentina">Fiorentina</a>&nbsp;(on loan) followed by&nbsp;<a href="https://en.wikipedia.org/wiki/A.S._Roma">Roma</a>. In 2017 he transferred to Liverpool for a then club record fee. He broke a record of scoring the most goals in a debut season for Liverpool, and has twice been awarded the&nbsp;<a href="https://en.wikipedia.org/wiki/Premier_League_Player_of_the_Month">Premier League Player of the Month</a>, November 2017 and February 2018.<a href="https://en.wikipedia.org/wiki/Mohamed_Salah#cite_note-feb18-4">[4]</a></p>\r\n\r\n<p>At international level, Salah represented Egypt at youth level, winning a bronze medal in the&nbsp;<a href="https://en.wikipedia.org/wiki/Africa_U-20_Cup_of_Nations">Africa U-20 Cup of Nations</a>, and participated in the&nbsp;<a href="https://en.wikipedia.org/wiki/2011_FIFA_U-20_World_Cup">2011 FIFA U-20 World Cup</a>&nbsp;and the&nbsp;<a href="https://en.wikipedia.org/wiki/2012_Summer_Olympics">2012 Summer Olympics</a>. He was awarded the&nbsp;<a href="https://en.wikipedia.org/wiki/CAF_Awards">CAF Most Promising African Talent</a>&nbsp;of the Year in 2012.<a href="https://en.wikipedia.org/wiki/Mohamed_Salah#cite_note-cafonline_2012-12-21-5">[5]</a>&nbsp;Making his debut with the senior national team in 2011, he helped Egypt reach the final of the&nbsp;<a href="https://en.wikipedia.org/wiki/2017_Africa_Cup_of_Nations">2017 Africa Cup of Nations</a>, and he became the top scorer of&nbsp;<a href="https://en.wikipedia.org/wiki/2018_FIFA_World_Cup_qualification_(CAF)">FIFA World Cup qualification (CAF)</a>&nbsp;in helping the team qualify for the&nbsp;<a href="https://en.wikipedia.org/wiki/2018_FIFA_World_Cup">2018 FIFA World Cup</a>.</p>\r\n\r\n<p>In 2017, Salah was named&nbsp;<a href="https://en.wikipedia.org/wiki/CAF_African_Footballer_of_the_Year">CAF African Footballer of the Year</a><a href="https://en.wikipedia.org/wiki/Mohamed_Salah#cite_note-6">[6]</a>&nbsp;and the&nbsp;<a href="https://en.wikipedia.org/wiki/BBC_African_Footballer_of_the_Year">BBC African Footballer of the Year</a>.<a href="https://en.wikipedia.org/wiki/Mohamed_Salah#cite_note-7">[7]</a>&nbsp;He was also selected in the&nbsp;<a href="https://en.wikipedia.org/wiki/CAF_Team_of_the_Year">CAF Team of the Year</a>&nbsp;and&nbsp;<a href="https://en.wikipedia.org/wiki/2017_Africa_Cup_of_Nations">CAF Africa Cup of Nations Team of the Tournament</a>.</p>\r\n', '220px-Mohamed_Salah_2017.jpg', '2018-03-31 22:58:03'),
(3, 5, 3, 1, 'Blog Post 5', 'Blog-Post-5', '<p><strong>Stereoscopic 3D television</strong>&nbsp;was demonstrated for the first time on 10 August 1928, by&nbsp;<a href="https://en.wikipedia.org/wiki/John_Logie_Baird">John Logie Baird</a>&nbsp;in his company&#39;s premises at 133 Long Acre, London.<a href="https://en.wikipedia.org/wiki/3D_television#cite_note-3">[3]</a>&nbsp;Baird pioneered a variety of 3D television systems using electro-mechanical and cathode-ray tube techniques. The first 3D TV was produced in 1935, and stereoscopic 3D still cameras for personal use had already become fairly common by the Second World War. Many 3D movies were produced for theatrical release in the US during the 1950s just when television started to become popular. The first such movie was&nbsp;<em><a href="https://en.wikipedia.org/wiki/Bwana_Devil">Bwana Devil</a></em>&nbsp;from&nbsp;<a href="https://en.wikipedia.org/wiki/United_Artists">United Artists</a>&nbsp;that could be seen all across the US in 1952. One year later, in 1953, came the 3D movie&nbsp;<em><a href="https://en.wikipedia.org/wiki/House_of_Wax_(1953_film)">House of Wax</a></em>which also featured&nbsp;<a href="https://en.wikipedia.org/wiki/Stereophonic_sound">stereophonic sound</a>.&nbsp;<a href="https://en.wikipedia.org/wiki/Alfred_Hitchcock">Alfred Hitchcock</a>&nbsp;produced his film&nbsp;<em><a href="https://en.wikipedia.org/wiki/Dial_M_for_Murder">Dial M for Murder</a></em>&nbsp;in 3D, but for the purpose of maximizing profits the movie was released in 2D because not all cinemas were able to display 3D films. In 1946 the&nbsp;<a href="https://en.wikipedia.org/wiki/Soviet_Union">Soviet Union</a>&nbsp;also developed 3D films, with&nbsp;<em><a href="https://en.wikipedia.org/wiki/Robinzon_Kruzo">Robinzon Kruzo</a></em>&nbsp;being its first full-length 3D movie.<a href="https://en.wikipedia.org/wiki/3D_television#cite_note-4">[4]</a>&nbsp;People were excited to view the 3D movies, but were put off by their poor quality. Because of this, their popularity declined quickly. There was another attempt in the 1970s and 80s to make 3D movies more mainstream with the releases of&nbsp;<a href="https://en.wikipedia.org/wiki/Friday_the_13th_Part_III">Friday the 13th Part III</a>&nbsp;(1982) and&nbsp;<a href="https://en.wikipedia.org/wiki/Jaws_3-D">Jaws 3-D</a>&nbsp;(1983).<a href="https://en.wikipedia.org/wiki/3D_television#cite_note-5">[5]</a>&nbsp;3D showings became more popular throughout the 2000s, culminating in the success of 3D presentations of&nbsp;<a href="https://en.wikipedia.org/wiki/Avatar_(2009_film)">Avatar</a>&nbsp;in December 2009 and January 2010.<a href="https://en.wikipedia.org/wiki/3D_television#cite_note-6">[6]</a></p>\r\n\r\n<p>Though 3D movies were generally well received by the public, 3D television did not become popular until after the&nbsp;<a href="https://en.wikipedia.org/wiki/Consumer_Electronics_Show">CES</a>&nbsp;2010 trade show, when major manufacturers began selling a full lineup of 3D televisions.<a href="https://en.wikipedia.org/wiki/3D_television#cite_note-7">[7]</a>&nbsp;According to DisplaySearch, 3D television shipments totaled 41.45 million units in 2012, compared with 24.14 in 2011 and 2.26 in 2010.<a href="https://en.wikipedia.org/wiki/3D_television#cite_note-8">[8]</a>&nbsp;In late 2013, the number of 3D TV viewers started to decline,<a href="https://en.wikipedia.org/wiki/3D_television#cite_note-9">[9]</a><a href="https://en.wikipedia.org/wiki/3D_television#cite_note-10">[10]</a><a href="https://en.wikipedia.org/wiki/3D_television#cite_note-11">[11]</a><a href="https://en.wikipedia.org/wiki/3D_television#cite_note-12">[12]</a><a href="https://en.wikipedia.org/wiki/3D_television#cite_note-13">[13]</a>&nbsp;and as of 2016, development of 3D TV is limited to a few premium models.<a href="https://en.wikipedia.org/wiki/3D_television#cite_note-14">[14]</a></p>\r\n', 'Active-3d-shutter-technology.gif', '2018-03-31 23:01:26'),
(4, 5, 1, 2, 'Blog Post 3', 'Blog-Post-3', '<p>The&nbsp;<code>$config</code>&nbsp;array contains your configuration variables. It is passed to the&nbsp;<code>$this-&gt;pagination-&gt;initialize()</code>&nbsp;method as shown above. Although there are some twenty items you can configure, at minimum you need the three shown. Here is a description of what those items represent:</p>\r\n\r\n<ul>\r\n	<li><strong>base_url</strong>&nbsp;This is the full URL to the controller class/function containing your&nbsp;pagination. In the example above, it is pointing to a controller called &ldquo;Test&rdquo; and a function called &ldquo;page&rdquo;. Keep in mind that you can&nbsp;<a href="https://www.codeigniter.com/userguide3/general/routing.html">re-route your URI</a>&nbsp;if you need a different structure.</li>\r\n	<li><strong>total_rows</strong>&nbsp;This number represents the total rows in the result set you are creating&nbsp;pagination&nbsp;for. Typically this number will be the total rows that your database query returned.</li>\r\n	<li><strong>per_page</strong>&nbsp;The number of items you intend to show per page. In the above example, you would be showing 20 items per page.</li>\r\n</ul>\r\n', 'noimage.jpg', '2018-04-01 18:40:11'),
(6, 6, 2, 2, 'Post blog 6', 'Post-blog-6', '<p>Some quick example text to build on the card title and make up the bulk of the card&#39;s content.</p>\r\n', 'download.jpg', '2018-04-07 15:07:16');

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE IF NOT EXISTS `provinces` (
  `province_id` int(11) NOT NULL AUTO_INCREMENT,
  `province_name` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`province_id`, `province_name`, `category_id`, `country_id`) VALUES
(1, 'Phnom Penh', 1, 1),
(2, 'Kandal', 1, 1),
(3, 'Battambang', 2, 1),
(4, 'Banteay Meanchey', 2, 1),
(5, 'Siem Reap', 3, 1),
(6, 'Kampong Cham', 3, 1),
(7, 'Preah Sihanouk', 0, 1),
(8, 'Banteay Meanchey', 0, 1),
(9, 'Kampong Speu', 0, 1),
(10, 'Kampot', 0, 1),
(11, 'Phetchaburi', 0, 2),
(12, 'Ratchaburi', 0, 2),
(13, 'Samutsakhon', 0, 2),
(14, 'Nonthaburi', 0, 2),
(15, 'Nakhonpathom', 0, 2),
(16, 'Bangkok', 0, 2),
(17, 'Samutprakan', 0, 2),
(18, 'Samutsongkhram', 0, 2),
(19, 'Beijing Municipality', 0, 3),
(20, 'Tianjin Municipality', 0, 3),
(21, 'Hebei', 0, 3),
(22, 'Shanxi', 0, 3),
(23, 'Inner Mongolia Autonomous Region', 0, 3),
(24, 'Liaoning', 0, 3),
(25, 'Jilin', 0, 3),
(26, 'Heilongjiang', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `zipcode`, `email`, `username`, `password`, `register_date`) VALUES
(1, 'Admin', '78945612', 'admin@admin.com', 'admin', 'f6fdffe48c908deb0f4c3bd36c032e72', '2018-04-03 17:49:50'),
(2, 'Osama arafa', '44568', 'osamaarafa96@yahoo.com', 'osamaarafa96', '601f1889667efaebb33b8c12572835da3f027f78', '2018-04-01 17:51:29'),
(3, 'Ali Modamed', '1254', 'ali_m15@gmail.com', 'ali_m15', '733d7be2196ff70efaf6913fc8bdcabf', '2018-04-01 13:07:54'),
(66, 'Modamed Ali', '15d62', 'mohamed@gmail.com', 'mohamedali', '4297f44b13955235245b2497399d7a93', '2018-04-10 17:19:45'),
(67, 'osmaarafa', 'dasxd', 'osamaarafa15@gmial.com', 'osamaarafa9', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2018-04-15 18:31:09'),
(68, 'jlkljk', 'gccnj', 'osamaarafa6@yahoo.co', 'osamaarafa', '4297f44b13955235245b2497399d7a93', '2018-04-15 18:57:08');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
