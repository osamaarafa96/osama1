<?php

class Comments extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->view('templates/header');
  }

  public function create($post_id)
  {
    // Check logged_in
    if(!$this->session->userdata('logged_in')){
      redirect('users/login');
    }

    $slug = $this->input->post('slug');
    $data['post'] = $this->post_model->get_posts($slug);

    $this->form_validation->set_rules('body', 'Body', 'required');

    if($this->form_validation->run() === FALSE){

      $this->load->view('posts/view/'.$slug, $data);
      $this->load->view('templates/footer');
      redirect('posts/'.$slug);
    } else {
      $this->comment_model->create_comment($post_id);
      redirect('posts/'.$slug);
    }
  }

  public function delete($id)
  {
    // Check logged_in
    if(!$this->session->userdata('logged_in')){
      redirect('users/login');
    }

    $this->comment_model->delete_comment($id);
    redirect('posts/'.$slug);
  }

  public function edit($id)
  {
    // Check logged_in
    if(!$this->session->userdata('logged_in')){
      redirect('users/login');
    }

    $id = $this->input->post('id');
    $data['comment'] = $this->comment_model->get_comment($id);


      $this->load->view('comments/edit', $data);
      $this->load->view('templates/footer');
  }

  public function update($id)
  {
    // Check logged_in
    if(!$this->session->userdata('logged_in')){
      redirect('users/login');
    }

    $this->comment_model->update_comment();

    $this->session->set_flashdata('comment_updated' ,'Your comment has been updated');

      redirect('posts/'.$slug);
  }

}

 ?>
