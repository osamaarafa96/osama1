<?php

class Categories extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();

  }
 private  function test($data=array()){

              echo "<pre>";

        print_r($data);

        echo "</pre>";

        die;

    }
  public function index()
  {
    $data['title'] = 'Categories';

    $data['categories'] = $this->Category_model->get_categories();
    $ids = $this->Category_model->get_category_id();
    $data['posts'] = $this->post_model->get_posts(FALSE);


    $data['subview'] = 'categories/index';
    $this->load->view('admin_index',$data);
  }

  public function ahmed(){
	  $this->test($this->post_model->select_all());
  }


  public function create()
  {
    // Check logged_in
    if(!$this->session->userdata('logged_in')){
      redirect('users/login');
    }

    $data['title'] = 'Create Category';

    $this->form_validation->set_rules('name', 'Name', 'required');

    if($this->form_validation->run() === FALSE)
    {

      $data['subview'] = 'categories/create';
      $this->load->view('admin_index',$data);

    } else{
      $this->Category_model->create_category();

      $this->session->set_flashdata('Category_created' ,'Your category has been created');

      redirect('categories');
    }
  }

  public function posts($id)
  {
    $data['title'] = $this->Category_model->get_category($id)->name;

    $data['posts'] = $this->post_model->get_posts_by_category($id);

    $data['subview'] = 'posts/index';
    $this->load->view('admin_index',$data);
  }

  public function delete($id)
  {
      // Check logged_in
      if(!$this->session->userdata('logged_in')){
        redirect('users/login');
      }

      $this->Category_model->delete_category($id);
      //set message
      $this->session->set_flashdata('category_deleted' ,'Your category has been deleted');
      redirect('categories');
  }

   // Master Layout
  public function page_index()
  {
    $data['subview'] = "";
    $this->load->view('admin_index',$data);
  }



}

 ?>
