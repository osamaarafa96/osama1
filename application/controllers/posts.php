<?php
class Posts extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();


  }


  public function index($offset = 0)
  {
    //pagination Config
    $config['base_url'] = base_url() . 'posts/index/';
    $config['total_rows'] = $this->db->count_all('posts');
    $config['per_page'] = 3;
    $config['uri_segment'] = 3;
    // Produces: class="myclass"
    $config['attributes'] = array('class' => 'pagination-link');

    // Init pagination
    $this->pagination->initialize($config);

    $data['title'] = 'Lastest Posts';
    $data['posts'] = $this->post_model->get_posts(FALSE, $config['per_page'], $offset);

    $data['subview'] = 'posts/index';
    $this->load->view('admin_index', $data);

  }

  public function view($slug = NULL)
  {
    $data['post'] = $this->post_model->get_posts($slug);
    $post_id = $data['post']['id'];
    $data['comments'] = $this->comment_model->get_comments($post_id);

    if (empty($data['post'])) {
      show_404();
    }

    $data['title'] = $data['post']['title'];

    $data['subview'] = 'posts/view';
    $this->load->view('admin_index', $data);
  }

  public function create()
  {
    // Check logged_in
    if(!$this->session->userdata('logged_in')){
      redirect('users/login');
    }

    $data['title'] = 'Create Post';

    $data['categories'] = $this->post_model->get_categories();

    $this->form_validation->set_rules('title', 'Title', 'required|is_unique[posts.title]', array('is_unique' => 'This title is notvalid please type different one.'));
    $this->form_validation->set_rules('body', 'Body', 'required');

    if ($this->form_validation->run() === FALSE) {

      $data['subview'] = 'posts/create';
      $this->load->view('admin_index', $data);
    }else {
      //Upload image
        $config['upload_path'] = './assets/images/posts/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '3048';
        $config['max_width'] = '1200';
        $config['max_height'] = '1200';

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()) {
          $errors = array('error'=>$this->upload->display_errors());
          $post_image = 'noimage.jpg';
        }else{
          $data = array('upload_data' => $this->upload->data());
          $post_image = $_FILES['userfile']['name'];
        }
      $this->post_model->create_post($post_image);

      //set message
      $this->session->set_flashdata('post_created' ,'Your post has been created');

      redirect('posts');
    }
  }

  public function delete($id)
  {
      // Check logged_in
      if(!$this->session->userdata('logged_in')){
        redirect('users/login');
      }

      $this->post_model->delete_post($id);
      //set message
      $this->session->set_flashdata('post_deleted' ,'Your post has been deleted');
      redirect('posts');
  }

  public function edit($slug)
  {
    // Check logged_in
    if(!$this->session->userdata('logged_in')){
      redirect('users/login');
    }

    $data['post'] = $this->post_model->get_posts($slug);

    // Check user
    if($this->session->userdata('user_id') != $this->post_model->get_posts($slug)['user_id']){
      redirect('posts');
    }

    if (empty($data['post'])) {
      show_404();
    }

    $data['title'] = 'Edit Post';

    $data['categories'] = $this->post_model->get_categories();

    $data['subview'] = 'posts/edit';
    $this->load->view('admin_index', $data);
  }

  public function update()
  {
    // Check logged_in
    if(!$this->session->userdata('logged_in')){
      redirect('users/login');
    }

    $this->post_model->update_post();

    $this->session->set_flashdata('post_updated' ,'Your post has been updated');

      redirect('posts');
  }
}

 ?>
