<?php

class Regions extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
  }
  public function index()
   {
     $data['countries'] = $this->Dependent->get_country_query();


     $data['subview'] = 'Regions/index';
     $this->load->view('admin_index', $data);
   }

   public function get_province()
		{
			$this->load->model('Dependent');
			$country_id = $this->input->post('country_id');
			$provinces = $this->Dependent->get_province_query($country_id);
			if (count($provinces)>0) {
				$pro_select_box = '';
				$pro_select_box .= '<option value="">Select Province</option>';
				foreach ($provinces as $province) {
					$pro_select_box .= '<option value="' . $province->province_id . '">' . $province->province_name . '</option>';
				}
				echo $pro_select_box;
			}
		}

   public function get_province_post()
   {
     $this->load->model('Dependent','Category_model');
     $province_id = $this->input->post('province_id');
     $categories = $this->Category_model->get_categories();
     $province = $this->Dependent->get_province_name($province_id);
     $posts = $this->Dependent->get_province_post($province_id);
     $province_name = $province->province_name;
     if (count($posts)>0) {
       $data['categories'] = $categories;
       $data['province_name'] = $province_name;
       $data['posts'] = $posts;

       $this->load->view('regions/Posts_by_province', $data);

     } else {
       $this->load->view('regions/province_no_posts');
     }

   }
}


 ?>
