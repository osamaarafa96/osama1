<?php

class Post_model extends CI_Model{
  public function __construct()
  {
      parent::__construct();
      $this->load->database();
  }

  public function get_posts($slug = FALSE, $limit = FALSE, $offset = FALSE)
  {
    if ($limit) {
      $this->db->limit($limit, $offset);
    }
    if ($slug === FALSE) {
      $this->db->order_by('posts.id', 'DESC');
      $this->db->join('categories', 'categories.id = posts.category_id');
      $query = $this->db->get('posts');
      return $query->result_array();
    }
    $query = $this->db->get_where('posts', array('slug' => $slug));
    return $query->row_array();
  }
   //-----------------------------------------------------
   public function select_all(){
        $this->db->select('posts.category_id ,posts.id ,categories.name');
		    $this->db->join('categories', 'categories.id = posts.category_id',"left");
        $this->db->from('posts');
        $this->db->group_by("category_id");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
			$i=0;$data= $query->result();
            foreach( $query->result() as $row){
				$data[$i]->sub = $this->get_select_all($row->category_id);
				$i++;
			}
			return $data;
        }
        return false;
    }
   //-----------------------------------------------------
   public function get_select_all($search_key_value){
        $this->db->select('category_id ,id ,title');
        $this->db->from('posts');
        $this->db->where("category_id",$search_key_value);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }



  public function create_post($post_image)
  {
    $slug = url_title($this->input->post(title));

    $data = array(
        'title' => $this->input->post('title'),
        'slug' => $slug,
        'body' => $this->input->post('body'),
        'category_id' => $this->input->post('category_id'),
        'user_id' => $this->session->userdata('user_id'),
        'post_image' => $post_image
    );

    return $this->db->insert('posts', $data);
  }

  public function delete_post($id)
  {
    $this->db->where('id',$id);
    $this->db->delete('posts');
    return true;
  }

  public function update_post($post_image)
  {
    $slug = url_title($this->input->post('title'));

    $data = array(
        'title' => $this->input->post('title'),
        'slug' => $slug,
        'body' => $this->input->post('body'),
        'category_id' => $this->input->post('category_id')
    );
    $this->db->where('id',$this->input->post('id'));
    return $this->db->update('posts', $data);
  }

  public function get_categories()
  {
    $this->db->order_by('name');
    $query = $this->db->get('categories');
    return $query->result_array();
  }

  public function get_posts_by_category($category_id)
  {
    $this->db->order_by('posts.id', 'DESC');
    $query = $this->db->get_where('posts', array('category_id' => $category_id));
    return $query->result_array();
  }
}


?>
