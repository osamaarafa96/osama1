<?php

class Dependent extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  public function get_country_query()
  {
    $query= $this->db->get('countries');
    return $query->result();
  }

  public function get_province_query($country_id)
  {
      $query= $this->db->get_where('provinces', array('country_id'=>$country_id));
      return $query->result();

  }

  public function get_province_post($province_id)
  {
      $query= $this->db->get_where('posts', array('province_id'=>$province_id));
      return $query->result();

  }

  public function get_province_name($province_id)
  {
    $query= $this->db->get_where('provinces', array('province_id'=>$province_id));
    return $query->row();
  }

}

 ?>
