<?php

class User_model extends CI_Model
{
  function __construct()
  {
    $this->load->database();
    parent::__construct();
  }

  public function valid_username($username)
  {

    $query = $query = $this->db->select('*')->from('users')->where('username',$username)->get();
    return $query->row();
  }

  public function register($enc_password)
  {
    # User data array
    $data = array(
        'name' => $this->input->post('name'),
        'zipcode' => $this->input->post('zipcode'),
        'email' => $this->input->post('email'),
        'username' => $this->input->post('username'),
        'password' => $enc_password
        );

        // insert user_registered
        return $this->db->insert('users', $data);
  }
      // Login User
  public function login($username, $password)
  {
    // Validate
    $this->db->where('username', $username);
    $this->db->where('password', $password);

    $result = $this->db->get('users');

    if ($result->num_rows() == 1) {
      return $result->row(0);
    } else {
      return false;
    }
  }

  public function email_exists($email)
  {
    $sql = "select name, email from users where  email = '{$email}' limit 1";
    $result = $this->db->query($sql);
    $row = $result->row();
    return ($result->num_rows() === 1 && $row->email) ? $row->name : false;
  }

  public function verify_reset_password_code($email ,$code)
  {
    $sql = "select name, email from users where  email = '{$email}' limit 1";
    $result = $this->db->query($sql);
    $row = $result->row();

    if($result->num_rows() === 1){
      return ($code == md5($this->config->item('salt') . $row->name)) ? true : false;
    }else {
      return false;
    }
  }

  public function update_password()
  {
    $email = $this->input->post('email');
    $password  = md5($this->config->item('salt') . $this->input->post('password'));

    $sql = "update users set password = '{$password}' where email = '{$email}' limit 1";
    $this->db->query($sql);

    if($this->db->affected_rows() === 1){
      return true;
    }else {
      return false;
    }
  }

}

 ?>
