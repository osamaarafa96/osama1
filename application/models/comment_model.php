<?php

class Comment_model extends CI_Model
{
  function __construct()
  {
    $this->load->database();
    parent::__construct();
  }

  public function create_comment($post_id)
  {
    $data = array(
      'user_id'  => $this->session->userdata('user_id'),
      'post_id'  => $post_id,
      'username' => $this->session->userdata('username'),
      'body'     => $this->input->post('body')
    );

    return $this->db->insert('comments', $data);
  }

  public function get_comments($post_id)
  {
    $query = $this->db->get_where('comments', array('post_id' => $post_id));
    return $query->result_array();
  }

  public function get_comment($id)
  {
    $query = $this->db->get_where('comments', array('id' => $id));
    return $query->result_array();
  }

  public function delete_comment($id)
  {
    $this->db->where('id',$id);
    $this->db->delete('comments');
    return true;
  }

  public function update_comment($id)
  {
    $data = array(
        'body' => $this->input->post('body')
      );

    $this->db->where('id',$id);
    $this->db->update('comments',$data);
    return true;
  }


}



 ?>
