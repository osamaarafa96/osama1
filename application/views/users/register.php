
  <?php echo form_open('users/register', 'class="email"  id="form"'); ?>
        <div class="row">
          <div class="col-md-4 col-md-offset-4" style=" background: #e2e6ea;margin:auto;width:50%;">
          <br><h2 class="alert alert-dismissible alert-info"><?= $title ?></h2>
          <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" id="name_form" name="name" placeholder="Enter name">
            <div class="invalid-feedback" id="name_error"></div>
          </div>
          <div class="form-group">
            <label>Zipcode</label>
            <input type="text" class="form-control" id="zip_form" name="zipcode" placeholder="Enter zipcode">
            <div class="invalid-feedback" id="zip_error"></div>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" id="email_form" name="email" placeholder="Enter email" maxlength="30">
            <div class="invalid-feedback" id="email_error"></div>
            <div class="text-danger"><?php echo form_error('email'); ?></div>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" id="username_form" name="username" placeholder="Enter username"  maxlength="30">
            <div id="username_error"></div>
            <div class="text-danger"><?php echo form_error('username'); ?></div>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" id="password_form" name="password" placeholder="Enter password">
            <div class="invalid-feedback" id="password_error"></div>
          </div>
          <div class="form-group">
            <label>Confirm Password</label>
            <input type="password" class="form-control" id="conf_password_form" name="password2" placeholder="Confirm password">
            <div class="invalid-feedback" id="conf_password_error"></div>
          </div>
          <input type="submit" name="submit" class="btn btn-primary btn-block" value="Submit"><br>
        </div>
    </div>
  </form>
