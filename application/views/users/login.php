<?php echo form_open('users/login'); ?>
    <div class="row" style="height:500px">
      <div class="col-md-4 col-lg-5" style=" background: #e2e6ea;margin:auto;width:50%;">
        <br>  <h3 class="alert alert-dismissible alert-info" style="text-align:center;"><?php echo $title; ?></h3>
          <div class="form-group">
            <input type="text" name="username" class="form-control" placeholder="Enter Username" required autofocus>
          </div>
          <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Enter Password" required autofocus>
          </div>
          <button type="submit" class="btn btn-primary btn-block">Login</button>
          if you do not have any account please sign up from <a  href="<?= base_url() ?>users/register">here!</a>
          <div><a href="<?php echo base_url(); ?>users/reset_password">Forget your password?</a></div>
      </div>
    </div>
</form>
