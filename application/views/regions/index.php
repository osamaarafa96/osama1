
<div class="row">
  <div >
    <h4>Countries</h4>
      <select  class="btn btn-info" name="country" id="country">
        <option value="">Select Category</option>
        <?php foreach ($countries as $country): ?>
            <option value="<?php echo $country->country_id; ?>"><?php echo $country->country_name; ?></option>
        <?php endforeach; ?>
      </select>
  </div>
  <div >
    <h4>Province</h4>
      <select  class="btn btn-info" name="province" id="province" disabled="">
        <option value="">Select Province</option>
      </select>

  </div><hr>
  <div class="row">
    <h4>Posts</h4>
    <table id="table" class="table table-hover">
      <thead><tr><th scope="col">Categories</th><th scope="col">Province</th><th scope="col">Posts</th><tr></thead>

    </table>
  </div>
</div>
