<h2><?php echo $post['title']; ?></h2>
<small class="post-date">Posted on: <?= $post['created_at']?></small>
<div class="col-md-3">
  <img src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image']; ?>">
</div>
<div class="post-body">
  <?= $post['body'] ?>
  </div>
<?php if($this->session->userdata('user_id') == $post['user_id']) : ?>
<hr>
<?php echo form_open('/posts/delete/'.$post['id']); ?>
<a class="btn btn-secondary" href="<?= base_url() ?>posts/edit/<?= $post['slug'] ?>">Edit</a>
    <input type="submit" value="delete" class="btn btn-danger" onclick="if(!confirm('Do you want delete the post')) return false;">
</form>
<?php endif; ?>
<hr>
<h3>Comments</h3>
<?php if($comments) : ?>
   <?php foreach ($comments as $comment) : ?>
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo $comment['username']; ?></h5>
      </div>
      <div class="modal-body">
        <p><?php echo $comment['body']; ?></p>
      </div>
      <?php if($this->session->userdata('user_id') == $comment['user_id']) : ?>
      <div class="modal-footer">
        <!-- <a class="badge badge-pill badge-secondary" href="<?= base_url() ?>comments/edit/<?= $comment['id'] ?>">Edit</a> -->
        <a class="badge badge-pill badge-danger" href="<?= base_url() ?>comments/delete/<?= $comment['id'] ?>" onclick="if(!confirm('Do you want delete the comment')) return false; ">Delete</a>
      </div>
    <?php endif; ?>
    </div>
   <?php endforeach; ?>
<?php else : ?>
    <p>No Comments To Display</p>
<?php endif; ?>
<?php if($this->session->userdata('logged_in')) : ?>
<hr>
<h4>Add Comment</h4>
<?php echo validation_errors(); ?>
<?php echo form_open('comments/create/'.$post['id']); ?>
    <div class="form-group">
      <label>Body</label>
      <textarea name="body" class="form-control"></textarea>
    </div>
    <input type="hidden" name="slug" value="<?php echo $post['slug']; ?>">
    <button type="submit" class="btn btn-primary">Comment</button>
</form>
<?php endif; ?>
