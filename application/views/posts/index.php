<h2><?= $title ?></h2>
<?php foreach ($posts as $post) : ?>
  <h3><?= $post['title'] ?></h3>
  <div class="row">
          <div class="col-md-3">
            <img class="post-thumb" src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image']; ?>">
          </div>
          <div class="col-md-3">
                <small class="post-date">Posted on: <?= $post['created_at']?></small>
                <?= word_limiter($post['body'], 20) ?>
                <br><br>
                <p><a class="btn btn-secondary"  href="<?php echo site_url('/posts/' .$post['slug']); ?>">Read More...</a></p>
          </div>
  </div>
<?php endforeach; ?>
<div class="pagination-links" >
<?php echo $this->pagination->create_links(); ?>
</div>
