<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>ciplog</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <script src="http://cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>







    $(document).ready(function(){

      $("#name_error").hide();
      $("#zip_error").hide();

      $("#email_error").hide();
      $("#password_error").hide();
      $("#conf_password_error").hide();

      var error_name = false;
      var error_zip = false;
      var error_username = false;
      var error_email = false;
      var error_password = false;
      var error_conf_password = false;


      function check_name() {

        var name_length = $("#name_form").val().length;

        if (name_length < 3) {
          $("#name_error").html("invalid name");
          $("#name_error").show();
          error_name = true;
        } else {
          $("#name_error").hide();
        }
      }

      function check_zip() {

        var zip_length = $("#zip_form").val().length;

        if (zip_length != 5) {
          $("#zip_error").html("invalid");
          $("#zip_error").show();
          error_zip = true;
        } else {
          $("#zip_error").hide();
        }
      }




      function check_email() {
        var email = $("#email_form").val();
        var filter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;


        if (!filter.test(email)) {
          $("#email_error").html("email notvalid");
          $("#email_error").show();
          error_email = true;
        } else {
          $("#email_error").hide();
        }
      }

      function check_password() {

        var password_length = $("#password_form").val().length;

        if (password_length < 6) {
          $("#password_error").html("Should be more than 6 letters");
          $("#password_error").show();
          error_password = true;
        } else {
          $("#password_error").hide();
        }
      }

      function check_conf_password() {

        var password = $("#password_form").val();
        var conf_password = $("#conf_password_form").val();

        if (password != conf_password) {
          $("#conf_password_error").html("Should be matchs");
          $("#conf_password_error").show();
          error_conf_password = true;
        } else {
          $("#conf_password_error").hide();
        }
      }





    // check name
    $("#name_form").focusout(function(){
        check_name();
    });

    // check zip code
    $("#zip_form").focusout(function(){
        check_zip();
    });

    // check username


    // check email
    $("#email_form").focusout(function(){
        check_email();
    });

    // check password
    $("#password_form").focusout(function(){
        check_password();
    });

    // matchs
    $("#conf_password_form").focusout(function(){
        check_conf_password();
    });




    // Check to submit
    $("#form").submit(function() {

            error_name = false;
            error_zip = false;
            error_email = false;
            error_password = false;
            error_conf_password = false;

            check_name();
            check_zip();
            check_email();
            check_password();
            check_conf_password();

            if(error_name == false && error_zip == false && error_email == false &&  error_password == false && error_conf_password == false){
              return true;
            }else {
              return false;
            }
});

// Dropdown dependent
$("#country").on('change', function(){
  var countryId = $(this).val();
  if(countryId == ''){
    $('#province').prop('disabled', true);
  } else {
    $('#province').prop('disabled', false);
    var  postData =  'country_id='+countryId;
    $.ajax({
      type: "POST",
      url:"<?= base_url() ?>Regions/get_province",
      data: postData,
      success: function(html){
        $("#province").html(html);
      },
      error: function () {
        alert("bcks");
      }
    });
  }
});

$("#province").on('change', function(){
  var provinceId = $(this).val();
  alert(provinceId);
  if(provinceId != ""){
    var  postData =  'province_id='+provinceId;

    $.ajax({
      type: "POST",
      url:"<?= base_url() ?>Regions/get_province_post",
      data: postData,
      success: function(html){
        alert(postData);
        $("#table").html(html);
      },
      error: function () {
        alert("bcks");
      }
    });
  }
});


$("#username_form").keyup(function hh(){
  var userName = $("#username_form").val().trim();
    var  postData =  'username='+userName;
    if(userName != ''){
      $('#username_error').show();
    $.ajax({
      type: "POST",
      url:"<?= base_url() ?>users/get_valid_username",
      data:postData ,
      success: function(response){
        if(response > 0){
                    $("#username_error").html("<span class='text-danger'> Username Already in use.</span>");
                }else{
                    $("#username_error").html("<span class='text-success'>Available.</span>");
                }
             },
      error: function () {
        alert("bcks");
      }
    });
  }else{
    $("#username_error").hide();
  }
});




    //check to submit
/*
var name = document.forms["myForm"]["name"].value;
var zip = document.forms["myForm"]["zipcode"].value;
var email = document.forms["myForm"]["email"].value;
var username = document.forms["myForm"]["username"].value;
var password = document.forms["myForm"]["password"].value;
var password2 = document.forms["myForm"]["password2"].value;
if(name.length < 3 || zip.length < 5 || username.length < 8 || password.length < 6 || password.value === password2.value){
check_name();
check_zip();
check_email();
check_password();
check_username();
check_conf_password();
return false;
}else {
return true;
}
}
*/
  });


  </script>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
        <a class="navbar-brand" href="<?php echo base_url(); ?>posts">ciBlog</a>
         <div class="collapse navbar-collapse" id="navbarColor02">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>posts">Posts</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>categories">Categories</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>regions">Regions</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>about">About</a>
            </li>
          </ul>
          <ul class="nav navbar-nav ml-auto">
            <?php if(!$this->session->userdata('logged_in')) : ?>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url() ?>users/login">Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url() ?>users/register">Register</a>
              </li>
          <?php endif; ?>
          <?php if($this->session->userdata('logged_in')) : ?>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url() ?>users/profile">Profile</a>
            </li>
           <li class="nav-item">
             <a class="nav-link" href="<?= base_url() ?>posts/create">Create Post</a>
           </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url() ?>categories/create">Create Category</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url() ?>users/logout">Logout</a>
          </li>
        <?php endif; ?>
        </ul>
      </div>
    </div>
  </nav>

</div>
<div class="container" style="padding:20px;">
      <!-- Flash messages -->
  <?php if($this->session->flashdata('user_registered')) : ?>
    <?php echo '<p class ="alert alert-dismissible alert-success">' . $this->session->flashdata('user_registered') . '</p>' ?>
  <?php endif; ?>

  <?php if($this->session->flashdata('post_created')) : ?>
    <?php echo '<p class ="alert alert-dismissible alert-success">' . $this->session->flashdata('post_created') . '</p>' ?>
  <?php endif; ?>

  <?php if($this->session->flashdata('post_updated')) : ?>
    <?php echo '<p class ="alert alert-dismissible alert-success">' . $this->session->flashdata('post_updated') . '</p>' ?>
  <?php endif; ?>

  <?php if($this->session->flashdata('category_created')) : ?>
    <?php echo '<p class ="alert alert-dismissible alert-success">' . $this->session->flashdata('user_registered') . '</p>' ?>
  <?php endif; ?>

  <?php if($this->session->flashdata('post_deleted')) : ?>
    <?php echo '<p class ="alert alert-dismissible alert-success">' . $this->session->flashdata('post_deleted') . '</p>' ?>
  <?php endif; ?>

  <?php if($this->session->flashdata('login_failed')) : ?>
    <?php echo '<p class ="alert alert-dismissible alert-danger">' . $this->session->flashdata('login_failed') . '</p>' ?>
  <?php endif; ?>

  <?php if($this->session->flashdata('user_loggedin')) : ?>
    <?php echo '<p class ="alert alert-success">' . $this->session->flashdata('user_loggedin') . '</p>' ?>
  <?php endif; ?>

  <?php if($this->session->flashdata('user_loggedout')) : ?>
    <?php echo '<p class ="alert alert-success">' . $this->session->flashdata('user_loggedout') . '</p>' ?>
  <?php endif; ?>

  <?php if($this->session->flashdata('category_deleted')) : ?>
    <?php echo '<p class ="alert alert-dismissible alert-success">' . $this->session->flashdata('category_deleted') . '</p>' ?>
  <?php endif; ?>
