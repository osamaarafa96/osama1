<h2><?= $title ?></h2>
<ul class="list-group">
  <?php foreach($categories as $category) : ?>
  <li class="alert alert-dismissible alert-info"><a href="<?php echo site_url('/categories/posts/'.$category['id']); ?>"><?= $category['name'] ?> Posts</a>
    <?php if($this->session->userdata('user_id') == $category['user_id']) : ?>
      <form class="cat-delete" action="categories/delete/<?php echo $category['id']; ?>" method="post">
        <input type="submit" class="btn-link text-danger" value="[X]">
      </form>

    <?php endif; ?>
  </li>

  <?php foreach ($posts as $post) : ?>
    <?php if($category['id'] === $post['category_id']) : ?>
    <h3><?= $post['title'] ?></h3>
    <div class="row">
            <div class="col-md-3">
              <img class="post-thumb" src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image']; ?>">
            </div>
            <div class="col-md-3">
                  <small class="post-date">Posted on: <?= $post['created_at']?></small>
                  <?= word_limiter($post['body'], 20) ?>
                  <br><br>
                  <p><a class="btn btn-secondary"  href="<?php echo site_url('/posts/' .$post['slug']); ?>">Read More...</a></p>
            </div>
    </div>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endforeach; ?>
</ul>
